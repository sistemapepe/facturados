import models from '../models';
import { model } from 'mongoose';
export default {
    add: async(req,res,next) => {
        try {
            const reg = await models.Empresa.create(req.body);
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error add empresa'
            });
            next(error);
        }
    },
    query: async(req,res,next) => {
        try {
            const reg = await models.Empresa.findOne({_id:req.query._id});
            if (!reg) {
                req.status(404).send({
                    message: 'El registro no existe'
                });
            } else {
                res.status(200).json(reg);
            }
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error query empresa'
            });
            next(error);
        }
    },
    list: async(req,res,next) => {
        try {
            let valor = req.query.valor;
            //const reg = await models.Empresa.find({$or:[{'nombre':new RegExp(valor,'i')},{'nit':valor}]},{_id:1,estado:1,nit:1,nombre:1});
            const reg = await models.Empresa.find({},{_id:1,estado:1,nit:1,nombre:1});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error list empresa'
            });
            next(error);
        }
    },
    update: async(req,res,next) => {
        try {
            const reg = await models.Empresa.findByIdAndUpdate({_id:req.body._id},{nit:req.body.nit, nombre:req.body.nombre, direccion:req.body.direccion, telefono:req.body.telefono, actividad:req.body.actividad, logo:req.body.logo, sigla:req.body.sigla});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error update empresa'
            });
            next(error);
        }
    },
    remove: async(req,res,next) => {
        try {
            const reg = await models.Empresa.findByIdAndDelete({_id:req.body._id});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error remove empresa'
            });
            next(error);
        }
    },
    activate: async(req,res,next) => {
        try {
            const reg = await models.Empresa.findByIdAndUpdate({_id:req.body._id},{estado:1});
            res.status(200).json(reg)
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error activate empresa'
            });
            next(error);
        }
    },
    deactivate: async(req,res,next) => {
        try {
            const reg = await models.Empresa.findByIdAndUpdate({_id:req.body._id},{estado:0});
            res.status(200).json(reg)
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error deactivate empresa'
            });
            next(error);
        }
    },
}