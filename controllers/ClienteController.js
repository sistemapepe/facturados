import models from '../models';

export default {
    add: async(req,res,next) => {
        try {
            const reg = await models.Cliente.create(req.body);
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error add Cliente'
            });
            next(error);
        }
    },
    query: async(req,res,next) => {
        try {
            const reg = await models.Cliente.findOne({_id:req.query._id})
            .populate('empresa',{nombre:1,nit:1});
            if (!reg) {
                req.status(404).send({
                    message: 'El registro no existe'
                });
            } else {
                res.status(200).json(reg);
            }
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error query Cliente'
            });
            next(error);
        }
    },
    list: async(req,res,next) => {
        try {
            let valor = req.query.valor;
            const reg = await models.Cliente.find({},{createdAt:0})
            .populate('empresa',{nombre:1,nit:1});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error list Cliente'
            });
            next(error);
        }
    },
    update: async(req,res,next) => {
        try {
            const reg = await models.Cliente.findByIdAndUpdate({_id:req.body._id},{nit:req.body.nit, nombre:req.body.nombre, empresa:req.body.empresa});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error update Cliente'
            });
            next(error);
        }
    },
    remove: async(req,res,next) => {
        try {
            const reg = await models.Cliente.findByIdAndDelete({_id:req.body._id});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error remove Cliente'
            });
            next(error);
        }
    },
    activate: async(req,res,next) => {
        try {
            const reg = await models.Cliente.findByIdAndUpdate({_id:req.body._id},{estado:1});
            res.status(200).json(reg)
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error activate Cliente'
            });
            next(error);
        }
    },
    deactivate: async(req,res,next) => {
        try {
            const reg = await models.Cliente.findByIdAndUpdate({_id:req.body._id},{estado:0});
            res.status(200).json(reg)
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error deactivate Cliente'
            });
            next(error);
        }
    },
}