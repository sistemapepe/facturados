import models from '../models';
import { model } from 'mongoose';

async function estadoDosificacion(iddosificacion, valor){
    const reg = await models.Dosificacion.findByIdAndUpdate({_id:iddosificacion},{estado:valor}); 
}

export default {
    add: async(req,res,next) => {
        try {
            req.body.fechaUfactura = req.body.fechaInicio;
            req.body.nroFacturas = 0;
            const reg = await models.Dosificacion.create(req.body);
            const lista = await models.Dosificacion.find({},{});
            lista.map(function(x){
                estadoDosificacion(x._id,0)
            })
            estadoDosificacion(reg._id,1)
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error add dosificacion'
            });
            next(error);
        }
    },
    query: async(req,res,next) => {
        try {
            const reg = await models.Dosificacion.findOne({_id:req.query._id})
            .populate('empresa',{nombre:1,nit:1});
            if (!reg) {
                req.status(404).send({
                    message: 'El registro no existe'
                });
            } else {
                res.status(200).json(reg);
            }
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error query dosificacion'
            });
            next(error);
        }
    },
    queryActivo: async(req,res,next) => {
        try {
            const reg = await models.Dosificacion.findOne({estado:1})
            .populate('empresa',{nombre:1,nit:1});
            if (!reg) {
                res.status(404).send({
                    message: 'No tiene activo ninguna dosificacion'
                });
            } else {
                res.status(200).json(reg);
            }
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error queryActivo dosificacion'
            });
            next(error);
        }
    },
    list: async(req,res,next) => {
        try {
            let valor = req.query.valor;
            const reg = await models.Dosificacion.find({},{createdAt:0})
            .sort({'fechaTramite':-1,'estado':-1})
            .populate('empresa',{nombre:1,nit:1});
            //,fecTramite:1,nroTramite:1,nroAutorizacion:1
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error list dosificacion'
            });
            next(error);
        }
    },
    update: async(req,res,next) => {
        try {
            const reg = await models.Dosificacion.findByIdAndUpdate({_id:req.body._id},{fechaTramite:req.body.fechaTramite, nroTramite:req.body.nroTramite, nroSucursal:req.body.nroSucursal, nroAutorizacion:req.body.nroAutorizacion, tipoFactura:req.body.tipoFactura, fechaInicio:req.body.fechaInicio, fechaFin:req.body.fechaFin, llave:req.body.llave, leyenda:req.body.leyenda, nroFacturas:req.body.nroFacturas, fechaUfactura:req.body.fechaUfactura, tipoImpresion:req.body.tipoImpresion, empresa:req.body.empresa, estado:req.body.estado});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error update dosificacion'
            });
            next(error);
        }
    },
    facturacion: async(req,res,next) => {
        try {
            const reg = await models.Dosificacion.findByIdAndUpdate({_id:req.body._id},{nroFacturas:req.body.nroFacturas, fechaUfactura:req.body.fechaUfactura});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error facturacion dosificacion'
            });
            next(error);
        }
    },
    remove: async(req,res,next) => {
        try {
            const reg = await models.Dosificacion.findByIdAndDelete({_id:req.body._id});
            res.status(200).json(reg);
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error remove dosificacion'
            });
            next(error);
        }
    },
    activate: async(req,res,next) => {
        try {
            const reg = await models.Dosificacion.findByIdAndUpdate({_id:req.body._id},{estado:1});
            res.status(200).json(reg)
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error activate dosificacion'
            });
            next(error);
        }
    },
    deactivate: async(req,res,next) => {
        try {
            const reg = await models.Dosificacion.findByIdAndUpdate({_id:req.body._id},{estado:0});
            res.status(200).json(reg)
        } catch (error) {
            res.status(500).send({
                message: 'Ocurrio un error deactivate dosificacion'
            });
            next(error);
        }
    },
}