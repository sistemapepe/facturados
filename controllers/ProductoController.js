import models from '../models';

export default {
    add: async (req,res,next) => {
        try{
            const reg = await models.Producto.create(req.body);
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error add producto"
            });
            next(e);
        }
    },
    query: async (req,res,next) => {
        try{
            const reg = await models.Producto.findOne({_id:req.query._id})
            .populate('categoria',{nombre:1,codigo:1});
            if (!reg){
                res.status(404).send({
                    message: 'El registro no existe'
                });
            } else{
                res.status(200).json(reg);
            }
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error query producto"
            });
            next(e);
        }
    },
    queryCodigo: async (req,res,next) => {
        try{
            const reg = await models.Producto.findOne({codigo:req.query.codigo})
            .populate('categoria',{nombre:1,codigo:1});
            if (!reg){
                res.status(404).send({
                    message: 'El registro no existe'
                });
            } else{
                res.status(200).json(reg);
            }
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error queryCod producto"
            });
            next(e);
        }
    },
    list: async (req,res,next) => {
        try{
            let valor = req.query.valor;
            const reg = await models.Producto.find({$or:[{'nombre':new RegExp(valor,'i')},{'descripcion':new RegExp(valor,'i')}]},{createdAt:0})
            .populate('categoria',{nombre:1,codigo:1,estado:1,unidadMedida:1})
            .sort({'codigo':1});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error list producto"
            });
            next(e);
        }
    },
    update: async (req,res,next) => {
        try{
            const reg = await models.Producto.findByIdAndUpdate({_id:req.body._id},{categoria:req.body.categoria,codigo:req.body.codigo,nombre:req.body.nombre,descripcion:req.body.descripcion,precioVenta:req.body.precioVenta,unidadMedida:req.body.unidadMedida,stock:req.body.stock});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error update producto"
            });
            next(e);
        }
    },
    remove: async (req,res,next) => {
        try{
            const reg = await models.Producto.findByIdAndDelete({_id:req.body._id});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error remove producto"
            });
            next(e);
        }
    },
    activate: async (req,res,next) => {
        try{
            const reg = await models.Producto.findByIdAndUpdate({_id:req.body._id},{estado:1});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error activate producto"
            });
            next(e);
        }
    },
    deactivate: async (req,res,next) => {
        try{
            const reg = await models.Producto.findByIdAndUpdate({_id:req.body._id},{estado:0});
            res.status(200).json(reg)
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error deactivate producto"
            });
            next(e);
        }
    },
}