import codcontrol from '../services/codigocontrol';

const fs = require('fs');
const readline = require('readline');

function leer(ruta, callback) {
    fs.readFile(ruta,'utf-8', (error, data) => {
        if (error){
            throw error;
        } else{
            console.log(data);
            return data;
        }
    })
}

export default{
    generar: async (req,res,next) => {
        try {
            // Generar codigo de control
            const codigoControl = await codcontrol.generar(req.body.numeroAutorizacion, req.body.numeroFactura, req.body.nitCliente, req.body.fechaEmision, req.body.total, req.body.llaveDosificacion);
            res.status(200).json(codigoControl);
        } catch (e) {
            res.status(500).send({
                message:'ocurrio un error en generar codigo de control'
            });
            next(e);
        }
    },
    cargardatos: async(req,res,next) => {
        console.log('descomentar la documentacion');
        // const ruta = __dirname + "/5000CasosPrueba.txt";
        // // const ruta = __dirname + "/casos.txt";
        // const rl = readline.createInterface({
        //     input: fs.createReadStream(ruta)
        // });
        // let index = 0;
        // let array = [];
        // let errores = [];
        // // Each new line emits an event - every time the stream receives \r, \n, or \r\n
        // rl.on('line', async (line) => {
        //     try {
        //         let arrayDosificacion = line.split('|');
        //         index=index+1;
        //         let codc = await codcontrol.generar(index,arrayDosificacion[0], arrayDosificacion[1], arrayDosificacion[2], arrayDosificacion[3], arrayDosificacion[4], arrayDosificacion[5], arrayDosificacion[10]);
        //         if(codc!==0) errores.push(codc)
        //         // console.log(index+" "+codc);
        //     } catch (error) {
        //         console.log(arrayDosificacion);
        //     }
            
        //     // array.push(arrayDosificacion);
        // });
        
        // rl.on('close', () => {
        //     console.log(errores);
        //     console.log('Done reading file');
        // });
        // console.log(array[0]);
    }
}

// NRO. AUTORIZACION|NRO. FACTURA|NIT/CI|FECHA EMISION|MONTO FACTURADO|LLAVE DOSIFICACION|5 VERHOEFF|CADENA|SUMATORIA PRODUCTOS|BASE64|CODIGO CONTROL