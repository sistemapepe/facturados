import models from '../models';

export default {
    add: async (req,res,next) => {
        try{
            const reg = await models.Categoria.create(req.body);
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error add categoria"
            });
            next(e);
        }
    },
    query: async (req,res,next) => {
        try{
            const reg = await models.Categoria.findOne({_id:req.query._id})
            .populate('empresa',{nombre:1,nit:1});
            if (!reg){
                res.status(404).send({
                    message: 'El registro no existe'
                });
            } else{
                res.status(200).json(reg);
            }
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error query categoria"
            });
            next(e);
        }
    },
    list: async (req,res,next) => {
        try{
            let valor = req.query.valor;
            const reg = await models.Categoria.find({$or:[{'nombre':new RegExp(valor,'i')},{'descripcion':new RegExp(valor,'i')}]},{createdAt:0})
            .populate('empresa',{nombre:1,nit:1})
            .sort({'createdAt':-1});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error list categoria"
            });
            next(e);
        }
    },
    update: async (req,res,next) => {
        try{
            const reg = await models.Categoria.findByIdAndUpdate({_id:req.body._id},{nombre:req.body.nombre,descripcion:req.body.descripcion, empresa:req.body.empresa});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error update categoria"
            });
            next(e);
        }
    },
    remove: async (req,res,next) => {
        try{
            const reg = await models.Categoria.findByIdAndDelete({_id:req.body._id});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error remove categoria"
            });
            next(e);
        }
    },
    activate: async (req,res,next) => {
        try{
            const reg = await models.Categoria.findByIdAndUpdate({_id:req.body._id},{estado:1});
            res.status(200).json(reg);
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error activate categoria"
            });
            next(e);
        }
    },
    deactivate: async (req,res,next) => {
        try{
            const reg = await models.Categoria.findByIdAndUpdate({_id:req.body._id},{estado:0});
            res.status(200).json(reg)
        } catch(e){
            res.status(500).send({
                message:"Ocurrio un error deactivate categoria"
            });
            next(e);
        }
    },
}