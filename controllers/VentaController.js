import models from '../models';
import codcontrol from '../services/codigocontrol';

async function aumentarStock(idproducto, cantidad){
    let {stock, unidadMedida} = await models.Producto.findOne({_id:idproducto});
    if (unidadMedida != 'SERVICIO'){
        let nSctock = parseInt(stock)+parseInt(cantidad);
        const reg = await models.Producto.findByIdAndUpdate({_id:idproducto},{stock:nSctock});
    }
}

async function disminuirStock(idproducto, cantidad){
    let {stock, unidadMedida} = await models.Producto.findOne({_id:idproducto});
    if (unidadMedida != 'SERVICIO'){
        let nSctock = parseInt(stock)-parseInt(cantidad);
        const reg = await models.Producto.findByIdAndUpdate({_id:idproducto},{stock:nSctock});
    }
}

export default{
    add: async (req,res,next) => {
        try {
            
            const dosificacion = await models.Dosificacion.findOne({_id:req.body.dosificacion});
            const empresa =  await models.Empresa.findOne({_id:req.body.empresa});
            // COMPLETAR DATOS DE LA VENTA
            req.body.especificacion = "3";
            req.body.numeroFactura = dosificacion.nroFacturas+1;
            req.body.numeroAutorizacion = dosificacion.nroAutorizacion;
            req.body.estado = "V";
            req.body.debitoFiscal = req.body.importeBase * 0.13;
            req.body.codigoControl = await codcontrol.generar(dosificacion.numeroAutorizacion, req.body.numeroFactura, req.body.nitCliente, req.body.fechaEmision, req.body.importeBase, dosificacion.llave);
            req.body.logoEmpresa = empresa.logo;
            req.body.direccionEmpresa = empresa.direccion;
            req.body.fechaLimite = dosificacion.fecFin;
            req.body.leyenda = dosificacion.leyenda;
            // GUARDAR VENTA
            const reg = await models.Venta.create(req.body);
            // Actualizar stock
            let detalle = req.body.detalle;
            detalle.map(function(x){
                disminuirStock(x._id,x.cantidad);
            });
            // ACTUALIZAR NUMEROFACTURA EN DOSIFICACION
            const regD = await models.Dosificacion.findByIdAndUpdate({_id:dosificacion._id},{nroFacturas:req.body.numeroFactura, fecUfactura:req.body.fechaEmision});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message:'ocurrio un error en crear Venta'
            });
            next(e);
        }
    },
    query: async (req,res,next) => {
        try {
            const reg = await models.Venta.findOne({_id:req.query._id})
            .populate('usuario',{nombre:1})
            .populate('empresa',{nit:1});
            if (!req){
                req.status(404).send({
                    message:'la venta no existe'
                });
            } else{
                res.status(200).json(reg);
            }
        } catch (e) {
            res.status(500).send({
                message:'ocurrio un error en query venta'
            });
            next(e);
        }
    },
    list: async (req,res,next) => {
        try {
            const reg = await models.Venta.find({},{numeroAutorizacion:1,numeroFactura:1,fechaEmision:1,nitCliente:1,nombreRazonSocial:1,importeBase:1,estado:1})
            .populate('usuario',{nombre:1})
            .populate('empresa',{nit:1})
            .sort({'createdAt':-1});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message:'ocurrio un error en list venta'
            });
            next(e);
        }
    },
    /*update: async (req,res,next) => {
        try {
            const reg = await models.Venta.findByIdAndUpdate({_id:req.body._id},{clasificador:req.body.clasificador,rfc:req.body.rfc,
            nit:req.body.nit,nombre:req.body.nombre,domicilio:req.body.domicilio,ciudad:req.body.ciudad,telefono:req.body.telefono,
            cp:req.body.cp,actividad:req.body.actividad,num_sucursal:req.body.num_sucursal,dom_sucursal:req.body.dom_sucursal,
            ciu_sucursal:req.body.ciu_sucursal,tel_sucursal:req.body.tel_sucursal,logo:req.body.logo});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message:'ocurrio un error en update Venta'
            });
            next(e);
        }
    },
    remove: async (req,res,next) => {
        try {
            const reg = await models.Venta.findByIdAndDelete({_id:req.body._id});
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message:'ocurrio un error en remove Venta'
            });
            next(e);
        }
    },*/
    // activate: async (req,res,next) => {
    //     try {
    //         const reg = await models.Venta.findByIdAndUpdate({_id:req.body._id},{estado:1});
    //         // Actualizar stock
    //         let detalle = reg.detalle;
    //         detalle.map(function(x){
    //             disminuirStock(x._id,x.cantidad);
    //         });
    //         res.status(200).json(reg);
    //     } catch (e) {
    //         res.status(500).send({
    //             message:'ocurrio un error en activate venta'
    //         });
    //         next(e);
    //     }
    // },
    // deactivate: async (req,res,next) => {
    //     try {
    //         const reg = await models.Venta.findByIdAndUpdate({_id:req.body._id},{estado:0});
    //         // Actualizar stock
    //         let detalle = reg.detalle;
    //         detalle.map(function(x){
    //             aumentarStock(x._id,x.cantidad);
    //         });
    //         res.status(200).json(reg);
    //     } catch (e) {
    //         res.status(500).send({
    //             message:'ocurrio un error en deactivate venta'
    //         });
    //         next(e);
    //     }
    // }
    anularFactura: async (req,res,next) => {
        try {
            const reg = await models.Venta.findByIdAndUpdate({_id:req.body._id},{estado:"A",anulador:req.body.anulador,fechaAnulacion:req.body.fechaAnulacion,motivoAnulacion:req.body.motivoAnulacion});
            // Actualizar stock
            let detalle = reg.detalle;
            detalle.map(function(x){
                aumentarStock(x._id,x.cantidad);
            });
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message:'ocurrio un error en deactivate venta'
            });
            next(e);
        }
    }
}