import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import path from 'path';
import mongoose from 'mongoose';
import router from './routes';

//Conexion a la base de datos MongoDB
mongoose.Promise = global.Promise;
//const dbUrl = process.env.MONGOOSE_URI?process.env.MONGOOSE_URI:'mongodb://localhost:27017/dbsistemafacturados';
const dbUrl = 'mongodb+srv://admin:dS3KU2UztAr4r9h@cluster0-fvl4n.mongodb.net/dbsistemafacturados?retryWrites=true&w=majority';
mongoose.connect(dbUrl, {useCreateIndex:true, useNewUrlParser: true, useUnifiedTopology: true})
    .then(mongoose => console.log(`Conectado a la base de datos en el puerto 27017`))
    .catch(err => console.error(err));
mongoose.set('useFindAndModify', false);

const app = express();
app.use(morgan('dev'));
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(express.static(path.join(__dirname,'public')))

app.use('/api',router);

app.set('port',process.env.PORT || 3000);

app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
})