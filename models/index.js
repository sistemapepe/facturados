import Empresa from './empresa';
import Categoria from './categoria';
import Producto from './producto';
import Cliente from './cliente';
import Dosificacion from './dosificacion';
import Usuario from './usuario';
import Venta from './venta';

export default {
    Empresa,
    Categoria,
    Producto,
    Cliente,
    Dosificacion,
    Usuario,
    Venta
}