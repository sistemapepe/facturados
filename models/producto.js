import mongoose,{Schema} from 'mongoose';

const productoSchema = new Schema({
    categoria: {type: Schema.ObjectId, ref:'categoria'},
    codigo: {type:String, maxlength:15, unique:true},
    nombre: {type:String, maxlength:50, required:true},
    descripcion:{type:String, maxlength:255},
    precioVenta: {type:Number, required:true, default:0.00},
    unidadMedida: {type:String, default:'UNIDAD'},
    stock: {type:Number, required:true},
    estado: {type:Number, default:1},
    createdAt: {type:Date, default:Date.now}
});

const Producto = mongoose.model('producto',productoSchema);
export default Producto;