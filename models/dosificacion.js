import mongoose,{Schema} from 'mongoose';

const dosificacionSchema = new Schema({
    fechaTramite: {type: Date, required:true},
    nroTramite: {type: Number, default:1},
    nroSucursal: {type: Number, default:0},
    nroAutorizacion: {type: Number, required:true},
    tipoFactura: {type: String, default:'COMPUTARIZADO'},
    fechaInicio: {type: Date, default:Date.now},
    fechaFin: {type: Date, default:Date.now},
    llave: {type: String, required:true},
    leyenda: {type: String, required:true},
    nroFacturas: {type: Number, default:0},
    fechaUfactura: {type: Date, default:Date.now},
    tipoImpresion: {type: String, maxlength: 20},
    empresa: {type: Schema.ObjectId, ref:'empresa'},
    estado: {type:Number, default:1},
    createdAt: {type:Date, default:Date.now}
});

const Dosificacion = mongoose.model('dosificacion',dosificacionSchema);
export default Dosificacion;