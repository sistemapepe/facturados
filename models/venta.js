import mongoose, {Schema} from 'mongoose';

const ventaSchema = new Schema({
    usuario:{type: Schema.ObjectId, ref:'usuario'},
    empresa:{type: Schema.ObjectId, ref:'empresa', required:true},
    dosificacion:{type: Schema.ObjectId, ref:'dosificacion', required:true},
    // LIBRO DE VENTAS
    especificacion:{type:Number, default:3},
    fechaEmision:{type:Date, default:Date.now},
    numeroFactura:{type:Number },
    numeroAutorizacion:{type:String },  //traer desde dosificacion
    estado:{type:String, default:"V"},
    nitCliente:{type:String, required:true},
    nombreRazonSocial:{type:String, required:true, default:""},
    importeTotal:{type:Number, default:0.00},
    importeICE:{type:Number, default:0.00},
    exportaciones:{type:Number, default:0.00},
    ventasGravadas:{type:Number, default:0.00},
    subTotal:{type:Number, default:0.00},
    descuento:{type:Number, default:0.00},
    importeBase:{type:Number, default:0.00},
    debitoFiscal:{type:Number, default:0.00},
    codigoControl:{type:String, required:true},
    //DATOS EMPRESA
    logoEmpresa:{type:String, default:""},  //traer desde empresa
    direccionEmpresa:{type:String, default:""}, //traer desde empresa
    fechaLimite:{type:Date, default:Date.now},
    //CONTROL
    // usuarioAnulador:{type: Schema.ObjectId, ref:'usuario'},
    usuarioAnulador:{type: String, default:""},
    fechaAnulacion:{type:Date},
    motivoAnulacion:{type:String},
    createdAt:{type:Date, default:Date.now},

    // NUEVO SISTEMA FACTURACION
    codigoMetodoPago:{type:String},
    numeroTarjeta:{type:String, default:""},
    leyenda:{type:String},
    detalle:[{
        _id:{
            type:String,
            required:true
        },
        codigoProducto:{
            type:String,
            required:true
        },
        descripcion:{
            type:String,
            required:true
        },
        cantidad:{
            type:Number,
            required:true
        },
        precioUnitario:{
            type:Number,
            required:true
        },
        MontoDescuento:{
            type:Number,
            default:0.00
        },
        subTotal:{
            type:Number,
            required:true,
        },
    }],
    
});

const Venta = mongoose.model('venta',ventaSchema);

export default Venta;