import mongoose,{Schema} from 'mongoose';

const clienteSchema = new Schema({
    nit:{type:Number, unique:true, required:true},
    nombre: {type: String, maxlength:50, required:true, uppercase:true},
    estado: {type:Number, default:1},
    empresa: {type: Schema.ObjectId, ref:'empresa'},
    createdAt: {type:Date, default:Date.now}
});

const Cliente = mongoose.model('cliente',clienteSchema);
export default Cliente;