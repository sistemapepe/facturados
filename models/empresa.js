import mongoose,{Schema} from 'mongoose';

const empresaSchema = new Schema({
    nit:{type:Number, unique:true, required:true},
    nombre:{type:String, maxlength:100, unique:true, required:true},
    direccion:{type:String, maxlength:200},
    telefono:{type:String, maxlength:30},
    actividad:{type:String, maxlength:200},
    logo:{type:String, maxlength:50},
    sigla:{type:String, maxlength:10},
    estado:{type:Number, default:1},
    createdAt:{type:Date, default:Date.now}
});

const Empresa = mongoose.model('empresa',empresaSchema);

export default Empresa;