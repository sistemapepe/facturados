var todos = 0;
async function InvierteN(cadena) {
    var x = cadena.length;
    var cadenaInvertida = "";
   
    while (x>=0) {
      cadenaInvertida = cadenaInvertida + cadena.charAt(x);
      x--;
    }
    return cadenaInvertida;
}

async function Verhoeff(cad){

	const mul = [ [0,1,2,3,4,5,6,7,8,9],
				[1,2,3,4,0,6,7,8,9,5],
				[2,3,4,0,1,7,8,9,5,6],
				[3,4,0,1,2,8,9,5,6,7],
				[4,0,1,2,3,9,5,6,7,8],
				[5,9,8,7,6,0,4,3,2,1],
				[6,5,9,8,7,1,0,4,3,2],
				[7,6,5,9,8,2,1,0,4,3],
				[8,7,6,5,9,3,2,1,0,4],
			    [9,8,7,6,5,4,3,2,1,0]
    ];
	const per = [ [0,1,2,3,4,5,6,7,8,9],
				[1,5,7,6,2,8,3,0,9,4],
				[5,8,0,3,7,9,6,1,4,2],
				[8,9,1,6,0,4,3,5,2,7],
				[9,4,5,3,1,2,6,8,7,0],
				[4,2,8,6,5,7,3,9,0,1],
				[2,7,9,3,8,0,6,4,1,5],
				[7,0,4,6,9,1,3,2,5,8]
    ];
	let inv = [0,4,3,2,1,5,6,7,8,9] , check = 0;
    let numeroInvertido = await InvierteN(cad);
	for(let i = 0 ; i < numeroInvertido.length ; i++){
        check = mul[check][per[((i + 1) % 8)][parseInt(numeroInvertido.charAt(i))]];
	}
	return inv[check];
}

async function GenerarVerhoeff(x, n){
	let s = "";
	while(n--){
		const v = await Verhoeff(x);
		x = x + v.toString();
		s = s + v;
	}
	return s;
}

async function CompletaCero(s){
    if(s.length == 1) s = '0' + s;
	return s;
}

async function Hexadecimal(num){
    if (num==0) return '0';
    var order = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/";
    var base = 16;
    var str = "", r;
    while (num) {
        r = num % base
        num -= r;
        num /= base;
        str = order.charAt(r) + str;
    }
    return str;
}

async function RC4(m, k){
	let State = new Array(256) , x = 0 , y = 0 , index1  = 0 , index2 = 0 , Nmen;
	let MensajeCifrado = "" ;

	for(let i = 0 ; i <= 255 ; i++){
		State[i] = i;
	}

	for(let i = 0 ; i <= 255 ; i++){
		index2 = (k.charCodeAt(index1) + State[i] + index2) % 256;
        let aux = State[i];
        State[i] = State[index2];
        State[index2] = aux;
		index1 = (index1 + 1) % k.length;
		//cout<<index1<<" "<<index2<<"\n";
	}

	for(let i = 0 ; i < m.length ; i++){
	    x = (x + 1) % 256;
        y = (State[x] + y) % 256;
        let aux = State[x];
        State[x] = State[y];
        State[y] = aux;
		Nmen = m.charCodeAt(i) ^ State[(State[x] + State[y]) % 256];
		//console.log('m',m.charCodeAt(i));
		//console.log('n',State[(State[x] + State[y]) % 256]);
        if(i) MensajeCifrado +=  '-';
        //console.log('Nmen',Hexadecimal(Nmen));
        const mensaje = await Hexadecimal(Nmen);
        const mensajec = await CompletaCero(mensaje);
		MensajeCifrado += mensajec;
	}

	return MensajeCifrado;
}

async function quitarGuion(s){
	let n = "";
	for(let i = 0 ; i < s.length ; i++)
		if(s.charAt(i) != '-') n = n + s.charAt(i);
	return n;
}


async function sumaascii(str, a , n){
	let ints = 0;
	for(let i = a; i < str.length; i = i + n){
        ints += str.charCodeAt(i);
    }
	return ints;
}

async function Base64(num) {
    var order = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/";
    var base = order.length;
    var str = "", r;
    while (num) {
        r = num % base
        num -= r;
        num /= base;
        str = order.charAt(r) + str;
    }
    return str;
}

export default {
    //function codigocontrol(sna, snf, snit, sfec, fmon, slld,sv,sc,ss,sb,cc){
    generar: async(sna, snf, snit, sfec, fmon, slld) => {
    // async function generar(sna, snf, snit, sfec, fmon, slld){
        // console.log("sna",sna);
        // console.log("snf",snf);
        // console.log("snit",snit);
        // console.log("sfec",sfec);
        // console.log("fmon",fmon);
        // console.log("slld",slld);
        let na, nf, nit, fec, mon, lld; //numAutorizacion numfactura fecha monto llaveDosificacion
        
        let parts = sfec.split('/');
        //dfec = new Date(parts[0], parts[1] - 1, parts[2]);
        na = sna;
        nf = snf;
        nit = snit;
        // fec = sfec; // AAAAMMDD
        fec = parts[0]+parts[1]+parts[2]; // AAAA/MM/DD
        // fec = parts[2]+parts[0]+parts[1]; // MM/DD/AAAA
        let imon = parseInt(Math.round(fmon));
        mon = imon.toString();
        lld = slld;

        // console.log('fec',fec);
        // console.log('mon',mon);

        /* PASO 1 */
        
        let vna, vnf, vnit, vfec, vmon, vlld, total, vtotal; //paso 1 //string
        vnf = await GenerarVerhoeff(nf , 2);
        vnit = await GenerarVerhoeff(nit , 2);
        vfec = await GenerarVerhoeff(fec , 2);
        vmon = await GenerarVerhoeff(mon , 2);
        // console.log('vnf',vnf);
        // console.log('vnit',vnit);
        // console.log('vfec',vfec);
        // console.log('vmon',vmon);
        nf += vnf;
        nit += vnit;
        fec += vfec;
        mon += vmon;

        total = parseInt(nf)+parseInt(nit)+parseInt(fec)+parseInt(mon);
        total  = total.toString();
        vtotal = await GenerarVerhoeff(total, 5);
        total = total + vtotal;
        // console.log('vtotal',vtotal);

        /* PASO 2 */

        let p = 0;
        na = na + lld.substr(p , parseInt(vtotal[0])+1);
        p += parseInt(vtotal[0])+1;
        nf = nf + lld.substr(p , parseInt(vtotal[1])+1);
        p += parseInt(vtotal[1])+1;
        nit = nit + lld.substr(p , parseInt(vtotal[2])+1);
        p += parseInt(vtotal[2])+1;
        fec = fec + lld.substr(p , parseInt(vtotal[3])+1);
        p += parseInt(vtotal[3])+1;
        mon = mon + lld.substr(p , parseInt(vtotal[4])+1);
        // console.log('lld',lld);
        // console.log('na',na);
        // console.log('nf',nf);
        // console.log('nit',nit);
        // console.log('fec',fec);
        // console.log('mon',mon);

        /* PASo 3 */
        
        let cadcon = na + nf + nit + fec + mon;
        let llc = lld + vtotal;
        // console.log('cadcon',cadcon);
        // console.log('llc',llc);

        let trc4 = await RC4(cadcon , llc);
        //console.log('trc4',trc4);
        trc4 = await quitarGuion(trc4);
        // console.log('trc4',trc4);
        
        /* PASO 4 */

        let st = await sumaascii(trc4, 0, 1);
        let sp1 = await sumaascii(trc4, 0, 5);
        let sp2 = await sumaascii(trc4, 1, 5);
        let sp3 = await sumaascii(trc4, 2, 5);
        let sp4 = await sumaascii(trc4, 3, 5);
        let sp5 = await sumaascii(trc4, 4, 5);
        
        // console.log('st',st);
        // console.log('sp1',sp1);
        // console.log('sp2',sp2);
        // console.log('sp3',sp3);
        // console.log('sp4',sp4);
        // console.log('sp5',sp5);

        /* PASO 5 */

        sp1 = (st * sp1)/(parseInt(vtotal[0])+1);
        sp2 = (st * sp2)/(parseInt(vtotal[1])+1);
        sp3 = (st * sp3)/(parseInt(vtotal[2])+1);
        sp4 = (st * sp4)/(parseInt(vtotal[3])+1);
        sp5 = (st * sp5)/(parseInt(vtotal[4])+1);
        st = parseInt(sp1) + parseInt(sp2) + parseInt(sp3) + parseInt(sp4) + parseInt(sp5);
        // console.log('st',st);
        let st64 = await Base64(st);
        // console.log('st64',st64);


        /* PASO 6 */

        let codControl = await RC4(st64 , llc);
        console.log('codigo control',codControl);
        // if(codControl !== scc){
        //     console.log('codigo control',index,codControl,scc);
        //     return index;
        // }
        // return 0;
        return codControl;
    }

    // generar('8004005263848','673173','1666188','2008/08/10','51330','PNRU4cgz7if)[tr#J69j=yCS57i=uVZ$n@nv6wxaRFP+AUf*L7Adiq3TT[Hw-@wt');

}