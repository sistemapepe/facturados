import React from 'react'
import {Title} from '../components/title'
import {Divider,Form,Button} from 'semantic-ui-react'
import '../css/login.css'     

// importar iconos color="green" icon={<Icon name="check circle outline" size="large"/>}
//se puede poner una imagen en login con <img src='ruta etc'


export default ({styles,handleClick,handleSubmit}) =>
{
    const args = {
    }


    const handleChange = (ev,input) =>{
        
        args[input.name] = input.value
        console.log(args)
        
        
    }

    

    return (
        
            <div style={styles.box}>
                <Title className='titlelogin'>FACTURADOS</Title>
                <Divider horizontal >$</Divider>

                <Form onSubmit={ (ev)=>handleSubmit(ev,args) }>
                    <Form.Field>
                        <label>Correo Electronico</label>
                        <Form.Input type="text" name="email" onChange={handleChange} placeholder='ejemplo:juanito@gmail.com' />
                    </Form.Field>
                        
                    <Form.Field>
                        <label>Constraseña</label>
                        <Form.Input type="password" name="password" onChange={handleChange} placeholder='ejemplo:123456789' />
                    </Form.Field>
                    <Button type='submit' positive fluid>Iniciar Sesión</Button>
                </Form>
                <div style={styles.box}>
                <a href=" " onClick={handleClick} >olvidaste tu contraseña?</a>
                </div>
            </div>
                  
            

    )
}