import React from 'react'
import {Divider} from 'semantic-ui-react'
import {Title} from '../components/title'

export default ({styles,handleClick}) =>
{
    return (
        <div>
            <div style={styles.box}>
                <Title>FACTURADOS</Title>
                <Divider horizontal >$</Divider>

                </div>
                  
            <div style={styles.box}>
                <a href=" " onClick={handleClick}>ingrese al sistema...</a>
            </div>
        </div>
    )
}