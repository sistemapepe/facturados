import React from 'react';
import '../css/login.css';
import {Grid} from 'semantic-ui-react'; 
import Ifactura from '../img/factura-login.jpg';
import {Title} from '../components/title';
import SignIn from './signin';
import LostPassword from './lostpassword';


const styles =
{
    grid: {
        height:'100%',
        width: '900px',
        margin: '0 auto',   
    },
    box:
    {
        backgroundColor: 'white',
        border: '1px solid #e6e6e6',
        textAling: 'center',
        marginBotton: '1em',
        padding: '1em',
    }
}

export default class Login extends React.Component
{
    state={
        showLogin:true,
        showLostPassword:false,
    }

    showLogin = (ev)=>{
        ev.preventDefault()
        this.setState({showLogin:true,showLostPassword:false})
    }

    showLostPassword = (ev)=>{
        ev.preventDefault()
        this.setState({showLogin:false,showLostPassword:true})
    }

    handleLogin = (ev,args)=>{
        console.log("--------------->")
        console.log(args)
    }

    render ()
    {
        const {showLogin,showLostPassword} = this.state;
        
        return (
            <div>
                <div className='title-wrapper'>
                    <Title>Iniciar Sesión</Title>
                </div>
            
            <Grid columns={2} verticalAlign='middle' centered style={styles.grid}>
                <Grid.Row>
                    <Grid.Column>
                        <img src={Ifactura} alt='imagen decorativa' className='gridwrapper'/>
                    </Grid.Column>
                    
                    <Grid.Column>
                        {showLogin && <SignIn styles={styles} handleClick={this.showLostPassword} handleSubmit={this.handleLogin}/>}
                        {showLostPassword && <LostPassword styles={styles} handleClick={this.showLogin}/>}                  
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            </div>
        )
    }
}

