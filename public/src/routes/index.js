import React from 'react'
import 'semantic-ui-css/semantic.min.css'
import Home from './home'
import Login from './login'

import {
    BrowserRouter,
    Route,
    Switch, 
} from 'react-router-dom';


export default ()=>(
    <BrowserRouter>
        <Switch>
            <Route path='/' exact component= {Home}/>
            <Route path='/login' exact component= {Login}/> 
        </Switch>
    </BrowserRouter>
)