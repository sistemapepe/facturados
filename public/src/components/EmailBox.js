import React, { Component } from 'react'

export class EmailBox extends Component
{
    render ()
    {
        return(  
                   <div class="field">
                    <label class="label">Email address</label>
                    <div class="control has-icons-left has-icons-right">
                        <input 
                            class="input is-primary" 
                            type="email" 
                            placeholder="Enter email" 
                            />

                        <span class="icon is-small is-left">
                        <i class="fas fa-envelope"></i>
                        </span>
                        <span class="icon is-small is-right">
                        <i class="fas fa-exclamation-triangle"></i>
                        </span>
                    </div>
                    </div>    
        )
    }
}