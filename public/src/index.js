import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import * as serviceWorker from './serviceWorker';
import Routes from './routes'
import 'bulma/css/bulma.css';
import './index.css';


ReactDOM.render(
  <React.StrictMode>
    <Routes/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

/*
  import {Title} from './components/title'
import {EmailBox} from './components/EmailBox' 
  import { PassBox } from './components/PassBox';
import {ButtonSubmit} from './components/Button'
import {ButtonExit} from './components/Button'


  <Title>SIGN IN</Title>
        <div className='EmailBox-wrapper'>
          <EmailBox/>
        </div>
        <div className='EmailBox-wrapper'>
          <PassBox/>
        </div>
        <div className='Button-wrapper'>
          <ButtonSubmit/>
        </div>
        <div className='Button-wrapper'>
          <ButtonExit/>
        </div>

*/