import routerx from 'express-promise-router';
import clienteController from '../controllers/ClienteController';
import auth from '../middlewares/auth';

const router = routerx();

router.post('/add',auth.verifyUsuario,clienteController.add);
router.get('/query',auth.verifyUsuario,clienteController.query);
router.get('/list',auth.verifyUsuario,clienteController.list);
router.put('/update',auth.verifyUsuario,clienteController.update);
router.delete('/remove',auth.verifyUsuario,clienteController.remove);
router.put('/activate',auth.verifyUsuario,clienteController.activate);
router.put('/deactivate',auth.verifyUsuario,clienteController.deactivate);

export default router;