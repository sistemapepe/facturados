import routerx from "express-promise-router";
import empresaRouter from './empresa';
import categoriaRouter from './categoria';
import productoRouter from './producto';
import clienteRouter from './cliente';
import usuarioRouter from './usuario';
import dosificacionRouter from './dosificacion';
import ventaRouter from './venta';
import verificarRouter from './verificar';

const router = routerx();

router.use('/empresa',empresaRouter);
router.use('/categoria',categoriaRouter);
router.use('/producto',productoRouter);
router.use('/cliente',clienteRouter);
router.use('/dosificacion',dosificacionRouter);
router.use('/usuario',usuarioRouter);
router.use('/venta',ventaRouter);
router.use('/verificar',verificarRouter);


export default router;