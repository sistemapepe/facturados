import routerx from 'express-promise-router'
import dosificacionController from '../controllers/DosificacionController.js'
import auth from '../middlewares/auth';

const router = routerx();

router.post('/add',auth.verifyAdministrador,dosificacionController.add);
router.get('/query',auth.verifyAdministrador,dosificacionController.query);
router.get('/queryActivo',auth.verifyVendedor,dosificacionController.queryActivo);
router.get('/list',auth.verifyAdministrador,dosificacionController.list);
router.put('/update',auth.verifyAdministrador,dosificacionController.update);
router.put('/facturacion',auth.verifyAdministrador,dosificacionController.facturacion);
router.delete('/remove',auth.verifyAdministrador,dosificacionController.remove);
router.put('/activate',auth.verifyAdministrador,dosificacionController.activate);
router.put('/deactivate',auth.verifyAdministrador,dosificacionController.deactivate);

export default router;