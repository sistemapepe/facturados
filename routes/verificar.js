import routerx from 'express-promise-router';
import auth from '../middlewares/auth';
import verificarController from '../controllers/VerificarController';
const router = routerx();

router.post('/generar',auth.verifyUsuario,verificarController.generar);
router.get('/cargar',auth.verifyUsuario,verificarController.cargardatos);

export default router;