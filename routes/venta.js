import routerx from 'express-promise-router';
import auth from '../middlewares/auth';
import ventaController from '../controllers/VentaController';
const router = routerx();

router.post('/add',auth.verifyVendedor,ventaController.add);
router.get('/query',auth.verifyVendedor,ventaController.query);
router.get('/list',auth.verifyVendedor,ventaController.list);
// router.get('/grafico12meses',auth.verifyUsuario,ventaController.grafico12Meses);
/*
router.put('/update',auth.verifyVendedor,ventaController.update);
router.delete('/remove',auth.verifyVendedor,ventaController.remove);
*/
// router.put('/activate',auth.verifyVendedor,ventaController.activate);
// router.put('/deactivate',auth.verifyVendedor,ventaController.deactivate);
router.put('/anularFactura',auth.verifyVendedor,ventaController.anularFactura);

export default router;